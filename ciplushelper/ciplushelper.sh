#!/bin/sh

# Configuration
#########################################
plugin="ciplushelper"
git_url="https://gitlab.com/eliesat/systemplugins/-/raw/main/ciplushelper"
version=$(wget $git_url/version -qO- | awk 'NR==1')
plugin_path="/usr/bin/ciplushelper"
package="enigma2-plugin-extensions-$plugin"
targz_file="$plugin.tar.gz"
url="$git_url/$targz_file"
temp_dir="/tmp"

# Determine package manager
#########################################
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
uninstall_command="apt-get purge --auto-remove -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
uninstall_command="opkg remove --force-depends"
fi

#check and_remove package old version
#########################################
check_and_remove_package() {
if [ -d $plugin_path ]; then
echo "> removing package old version please wait..."
sleep 3 
rm -rf $plugin_path > /dev/null 2>&1
if [ -e /etc/init.d/ciplushelper ]; then
	/etc/init.d/ciplushelper stop
fi
if [ -z "$D" -a -f "/etc/init.d/ciplushelper" ]; then
	/etc/init.d/ciplushelper stop
fi
if true && type update-rc.d >/dev/null 2>/dev/null; then
	if [ -n "$D" ]; then
		OPT="-f -r $D"
	else
		OPT="-f"
	fi
	update-rc.d $OPT ciplushelper remove
fi

if [ -e /usr/bin/ciplushelper ]; then
	rm /usr/bin/ciplushelper
fi
if [ -e /etc/init.d/ciplushelper ]; then
	rm /etc/init.d/ciplushelper
fi

if grep -q "$package" "$status_file"; then
echo "> Removing existing $package package, please wait..."
$uninstall_command $package > /dev/null 2>&1
fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3
exit 1
else
echo " " 
fi  }
check_and_remove_package

# Determine Python version
if [ -e /usr/bin/python3 ]; then
    PY=python3
else
    PY=python
fi

# Run BoxBrandingTest.py and process output
$PY /usr/lib/enigma2/python/BoxBrandingTest.py | sed 's/<$//' | sed 's/ /_/g' > /tmp/boxbranding.cfg

# Check for unsupported Box
if grep -qs 'getMachineBrand=xx' /tmp/boxbranding.cfg; then
    echo "                                 "
    echo "*********************************"
    echo "Not supported Box found! Aborted!"
    echo "*********************************"
    echo "                                 "
    exit 1
fi

# Function to stop and remove service
remove_service() {
    service_name="$1"
    if [ -z "$D" ] && [ -f "/etc/init.d/$service_name" ]; then
        /etc/init.d/"$service_name" stop
        update-rc.d -f -r "$D" "$service_name" remove
    fi
}

# Stop and remove ciplushelper service
remove_service "ciplushelper"

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
# Function to remove helper binaries
remove_helpers() {
    if [ -e "/usr/bin/$1" ]; then
        rm "/usr/bin/$1"
    fi
}

# Remove unnecessary helper binaries
remove_helpers "ciplushelper"


# Function to copy ciplushelper and set up init.d script
copy_ciplushelper() {
    cp "/var/local/$1/usr/bin/ciplushelper" /usr/bin/ciplushelper
    cp "/var/local/etc/init.d/ciplushelper" /etc/init.d/ciplushelper
    if true && type update-rc.d >/dev/null 2>/dev/null; then
        OPT="-s"
        if [ -n "$D" ]; then
            OPT="-r $D"
        fi
        update-rc.d $OPT ciplushelper defaults 50
    fi
}

# Check machine builds and versions
if grep -qs 'getMachineBuild=hd2400\|getMachineBuild=hd1500\|getMachineBuild=triplex\|getMachineBuild=h6\|getMachineBuild=et8000\|getMachineBuild=et10000' /tmp/boxbranding.cfg; then
    echo "Mutant/Triplex/H6/Xtrend found"
    if grep -qs 'getImageVersion=\(5\.[35]\|6\.[0-6]\|7\.[0-9]\|8\.[0-9]\)' /tmp/boxbranding.cfg; then
        echo "Imageversion 5.x or 6.x found"
        kernel_version=$(uname -r)
        echo "kernel = $kernel_version"
        if [ ${kernel_version:0:3} == 4.1 ]; then
            echo "New ci helper for mutant/Triplex/H6/Xtrend"
            copy_ciplushelper "mipsel32/6.x"
        else
            echo "Old ci helper for mutant/Triplex/H6/Xtrend"
            copy_ciplushelper "mipsel32/6.x"
        fi
    fi
elif grep -qs 'getMachineBuild=hd51\|getMachineBuild=vs1500\|getMachineBuild=h7\|getMachineBuild=h9combo\|getMachineBuild=h9twin\|getMachineBuild=8100s\|getMachineBuild=h10\|getMachineBuild=hd61\|getMachineBuild=h9combose\|getMachineBuild=pulse4k\|getMachineBuild=pulse4kmini' /tmp/boxbranding.cfg; then
    echo "hd51/vs1500/h7/h9combo/h9twin/8100s(pulse4k/pulse4kmini found"
    if grep -qs 'getImageVersion=\(5\.[35]\|6\.[0-6]\|7\.[0-9]\|8\.[0-9]\)' /tmp/boxbranding.cfg; then
        echo "Imageversion 5.x or 6.x found"
        kernel_version=$(uname -r)
        echo "kernel = $kernel_version"
        if [ ${kernel_version:0:3} == 4.1 ] || [ ${kernel_version:0:3} == 4.4 ]; then
            echo "New ci helper for hd51/vs1500/h7/8100s/h9combo/h9twin/h10/hd61/pulse4k/pulse4kmini"
            copy_ciplushelper "hd51/6.new"
        else
            echo "Old ci helper for hd51/vs1500/h7/8100s"
            copy_ciplushelper "hd51/6.x"
        fi
    fi
elif grep -qs 'getMachineBuild=cube' /tmp/boxbranding.cfg; then
    echo "cube found"
    if grep -qs 'getImageVersion=\(5\.[35]\|6\.[0-6]\|7\.[0-9]\|8\.[0-9]\)' /tmp/boxbranding.cfg; then
        echo "Imageversion 6.1 or newer found"
        copy_ciplushelper "cube/6.1"
    fi
elif grep -qs 'getMachineBuild=formuler1\|getMachineBuild=formuler3\|getMachineBuild=formuler4' /tmp/boxbranding.cfg; then
    if grep -qs 'getMachineBuild=formuler4turbo\|getMachineBuild=formuler1tc\|getMachineBuild=formuler3ip\|getMachineBuild=formuler4ip' /tmp/boxbranding.cfg; then
        echo "found formuler4turbo/formuler1tc/formuler3ip/formuler4ip"
    else
        echo "Formuler1/3/4 found"
        if grep -qs 'getImageVersion=\(5\.[35]\|6\.[0-6]\|7\.[0-9]\|8\.[0-9]\)' /tmp/boxbranding.cfg; then
            echo "Imageversion 5.x or 6.x found"
            kernel_version=$(uname -r)
            echo "kernel = $kernel_version"
            if [ ${kernel_version:0:3} == 4.1 ]; then
                echo "New ci helper for formuler 1/3/4"
                copy_ciplushelper "mipsel32/6.x"
            else
                echo "Old ci helper for formuler 1/3/4"
            fi
        fi
    fi
fi

# Clean up temporary files
rm -rf /var/local/etc
rm -rf /var/local/cube
rm -rf /var/local/hd51
rm -rf /var/local/mipsel32
rm -rf /tmp/boxbranding.cfg
  echo "> $plugin-$version package installed successfully"
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By ElieSat"
}
cleanup
    