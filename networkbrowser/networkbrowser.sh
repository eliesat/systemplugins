#!/bin/bash

# Configuration
plugin="networkbrowser-arm-3.12"
site="https://gitlab.com/eliesat/systemplugins/-/raw/main/networkbrowser"
temp_dir="/tmp"


print_message() {
    echo "[$(date +'%Y-%m-%d %H:%M:%S')] $1"
}

# Determine package manager and plugin extension
if command -v dpkg &> /dev/null; then
    package_manager="apt"
    install_command="dpkg -i --force-overwrite"
    uninstall_command="apt-get purge --auto-remove -y"
    status_file="/var/lib/dpkg/status"
    plugin_extension="deb"
else
    package_manager="opkg"
    install_command="opkg install --force-reinstall"
    uninstall_command="opkg remove --force-depends"
    status_file="/var/lib/opkg/status"
    plugin_extension="ipk"
fi

#determine and check url
url="$site/$plugin.$plugin_extension"
if wget -q --method=HEAD $url; then
  pluginextension=found
else
  print_message "> $plugin.$plugin_extension not found"
  print_message "> your device is not supported"
exit 1
fi

#check arch armv7l aarch64 mips 7401c0 sh4
arch=$(uname -m)
#check python version
python_version=$(python -c "import platform; print(platform.python_version())")
echo "> python version: $python_version "
echo "> arch=: $arch "
sleep 3

if [ "$arch" == "aarch64" ]; then
case $python_version in 
2.*|3.8.*|3.9.*|3.10.*|3.11.*|3.12.*)
echo "> your device is not supported"
sleep 3
exit 1
;;
esac
elif [ "$arch" == "mips" ]; then
case $python_version in 
2.*|3.8.*|3.9.*|3.10.*|3.11.*|3.12.*)
echo "> your device is not supported"
sleep 3
exit 1
;;
esac
elif [ "$arch" == "armv7l" ]; then
case $python_version in 
3.8.*|3.9.*|3.10.*|3.11.*|3.12.*)
echo "$arch $python_version";;
*)
echo "> your image python version: $python_version is not supported"
sleep 3
exit 1
;;
esac

# Functions
cleanup() {
    print_message "> Performing cleanup..."
    [ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
    rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
    print_message "> Cleanup completed."
}

check_and_install_package() {
    if grep -q "$plugin.$plugin_extension" "$status_file"; then
        print_message "> Removing existing $plugin.$plugin_extension package, please wait..."
        $uninstall_command $plugin.$plugin_extension
    fi

    print_message "> Downloading $plugin.$plugin_extension, please wait..."
    wget -q --show-progress $url -P "$temp_dir"
    if [ $? -ne 0 ]; then
        print_message "> Failed to download $plugin.$plugin_extension from $url"
        exit 1
    fi
sleep 3
    print_message "> Installing $plugin.$plugin_extension, please wait..."
    $install_command "$temp_dir/$plugin.$plugin_extension"
    if [ $? -eq 0 ]; then
        print_message "> $plugin.$plugin_extension installed successfully."
    else
        print_message "> Installation failed."
        exit 1
    fi
}

# Main
trap cleanup EXIT
check_and_install_package

else
echo "> your device is not supported"
sleep 3
exit 1
fi
